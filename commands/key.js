const inquirer = require('inquirer');
const color = require('color');
const KeyManager = require('../lib/KeyManager')
const {isRequired} = require('../utils/validation')
const key= {
  async  set (){
        const keyManager = new KeyManager();
        const input =await inquirer.prompt({
            type:'input',
            name:'key',
            message:"Enter the API Key .green ".green +"https://nomics.com",
            validate: isRequired
        });
        console.log('hello from set');
        const key = keyManager.setKey(input.key);
        if(key){
            console.log('API key  set '.blue);
        }
    },
    show(){
       try{
        const keyManager = new KeyManager();
        const key = keyManager.getKey();
        console.log('current api key',key.yellow)
        return key;
       }
       catch(err){
         console.log('error',err.message.red)
       }
    },
    remove(){
        console.log('hello from remove')
        try{
            const keyManager = new KeyManager();
            keyManager.deleteKey();
            console.log('key removed '.blue)
            return ;
           }
           catch(err){
             console.log('error',err.message.red)
           }
    }
}
module.exports = key;