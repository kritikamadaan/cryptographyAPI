const Configstore = require('Configstore');
const pkg = require('../package.json');

class KeyManager{
constructor(){
    this.conf = new Configstore(pkg.name);
}
setKey(key){
this.conf.set('apiKey',key);
return key;
} 
getKey(prop){
    const key = this.conf.get('apikey');
    if(!key){
        throw new Error('No api Key Found- Get a Key ')
    }
    return key;
}
deleteKey(prop){
    const key = this.conf.get('apikey');
    if(!key){
        throw new Error('No api Key Found- Get a Key ')
    }

    this.conf.delete('apiKey')
    return key;
}
}
module.exports = key;